# SPONGE

Simulation Package Toward Next GEneration of molecular modelling (SPONGE) is a simulation package of molecular modelling accelerated by GPUs, developed by Gao Group.

visit the official website of SPONGE for more information on the installation guides, tutorials, applications and contribution guides.

https://spongemm.cn/
